# Angular7- EXERCICE



## Get the code

```
$ https://gitlab.com/RAOELSON03/exercice-fronted.git
$ cd exercice-fronted
$ npm install
```

## Run the app

```
$ ng serve
```

## Development

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Authentification the app

```
 username : user 
 password : 1234
 
 username : user1 
 password : 1234
```

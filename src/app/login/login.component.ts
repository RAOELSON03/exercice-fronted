import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  
  urlState: string  = "";
  constructor(private authService: AuthenticationService,
    private router: Router, private route: ActivatedRoute) {
      this.route.queryParams.subscribe(params => {
        this.urlState = (params.redirect_uri)
    });
    }

  onLogin(user: any) {
    this.authService.login(user).subscribe(res => {
      let jwt = res.headers.get('Authorization');
      this.authService.userToken(jwt);
      if (this.authService.isAuthenticated()) {
        (this.urlState ? this.authService.redirectToPaniers() : this.router.navigateByUrl(''))
      }
    }, err => {
      alert("Veuillez vérifier votre login ou mot de passe !")
    })
  }

}

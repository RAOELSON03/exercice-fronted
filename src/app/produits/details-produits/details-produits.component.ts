import { Component, OnInit, Input, DoCheck } from "@angular/core";
import { ProduitService } from './../../services/produits.service';
import { PanierService } from './../../services/panier.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: "app-detailsproduits",
  templateUrl: './details-produits.component.html'
})

export class DetailsProduitsComponent implements OnInit {
  produit: any;
  id;

  constructor(private prdService: ProduitService,
    private route: ActivatedRoute,
    private panierService: PanierService) {
    this.id = (this.route.snapshot.paramMap.get("id"))
  }

  ngOnInit(): void {
    this.getDetailsProduits();
  }


  public getDetailsProduits() {
    this.prdService.getProduitDetails(this.id).subscribe(data => {
      this.produit = data;
      console.log(this.produit.name)
    }, err => {
      console.log(err);
    })
  }

  public onAddProductToCart(p) {
    this.panierService.addPanier(p);
  }
}

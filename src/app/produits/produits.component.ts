import { Component, OnInit, Input, DoCheck } from "@angular/core";
import { ProduitService } from './../services/produits.service';
import { CategorieService } from './../services/categories.service';
import { PanierService } from '../services/panier.service';

@Component({
  selector: "app-produits",
  templateUrl: './produits.component.html',
  styleUrls: ['./produits.component.css']
})

export class ProduitsComponent implements OnInit {
  produits: any;
  idParms;
  categorie: any;

  @Input('activeCategorie')
  set value(activeCategorie: any) {
    this.idParms = (activeCategorie)
    if (this.idParms) {
      this.getProduitCatBy(this.idParms)
    }
  }

  constructor(private prdService: ProduitService,
    private catService: CategorieService,
    private panierService: PanierService,) {
  }

  ngOnInit(): void {
    this.getProduits();
  }


  public getProduits() {
    this.prdService.getProduitList().subscribe(data => {
      this.produits = data._embedded.produits;
    }, err => {
      console.log(err);
    })
  }

  public getProduitCatBy(id) {
    this.catService.getProduitByCat(id).subscribe(data => {
      this.produits = data._embedded.produits;
    }, err => {
      console.log(err);
    })
  }

  public onAddProductToCart(p) {
    this.panierService.addPanier(p);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { JwtHelperService } from "@auth0/angular-jwt";
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  url: string = "";

  constructor(private http: HttpClient, private router: Router) {
    this.url = environment.hosts.url;
  }

  login(data: any) {
    const url = this.url + "/login";
    return this.http.post(url, data, { observe: 'response' });
  }

  userToken(jwt) {
    if (jwt) {
      const helper = new JwtHelperService();
      const objJWT = helper.decodeToken(jwt);
      localStorage.setItem("roles", objJWT.roles);
      localStorage.setItem("token", jwt);
    }
  }

  isAuthenticated() {
    return localStorage.getItem('roles');
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('roles');
  }

  redirectToLogin(url): boolean {
    this.router.navigate(['/login'], { queryParams: { redirect_uri: url } });
    return true;
  }

  redirectToPaniers(): boolean {
    this.router.navigate(['/panier']);
    return true;
  }

}

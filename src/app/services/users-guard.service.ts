import { Injectable } from '@angular/core';
import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UsersGuardService implements CanActivate {
    urlState: string = "";
    constructor(
        private authenticationService: AuthenticationService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, 
        state: RouterStateSnapshot): Observable<boolean> | boolean {
        this.urlState = state.url;
        const userRole = this.authenticationService.isAuthenticated()
        return userRole ? true : this.logOut();
    }

    logOut() {
        this.authenticationService.logout()
        return this.authenticationService.redirectToLogin(this.urlState);
    }
}
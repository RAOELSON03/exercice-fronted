import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ProduitService {
  url: string;
  constructor(private http: HttpClient) {
    this.url = environment.hosts.url;
  }

  getProduitList(): Observable<any> {
    const url = this.url + '/produits';
    return this.http.get(url);
  }

  getProduitDetails(id): Observable<any> {
    const url = this.url + `/produits/${id}`;
    return this.http.get(url);
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CategorieService {
  url: string;
  constructor(private http: HttpClient) {
    this.url = environment.hosts.url;
  }

  getcategorieList(): Observable<any> {
    const url = this.url + '/categories';
    return this.http.get(url);
  }

  getProduitByCat(id): Observable<any> {
    const url = this.url + `/categories/${id}/produits`;
    console.log(url)
    return this.http.get(url);
  }

}

import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { v4 as uuid } from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class PanierService {
  url: string;
  nombre = 1
  paniers: any = [];
  constructor(private http: HttpClient) {
    this.url = environment.hosts.url;
  }

  @Output() change: EventEmitter<Number> = new EventEmitter();

  addPanier(produit: any) {
    this.addToLocalStorage(produit);
    this.updateNbrePanier();
  }


  public addToLocalStorage(produit: any): void {
    const objProduit = {
      _id: uuid(),
      id: produit.id,
      name: produit.name,
      description: produit.description,
      prix: produit.prix,
      qunatity: produit.quantity
    }
    this.paniers.push(objProduit);
    let paniers = [];
    if (localStorage.getItem('paniers') === null) {
      paniers = [];
      paniers.push(objProduit);
      localStorage.setItem('paniers', JSON.stringify(paniers));
    } else {
      paniers = JSON.parse(localStorage.getItem('paniers'));
      paniers.push(objProduit);
      localStorage.setItem('paniers', JSON.stringify(paniers));
    }
  }

  public getListPaniers() {
    if (localStorage.getItem('paniers') === null) {
      this.paniers = [];
    } else {
      this.paniers = JSON.parse(localStorage.getItem('paniers'));
    }
    return this.paniers;
  }

  public getProduitInPanier() {
    const data = this.getListPaniers();
    return (data.length > 0 ? data.length = data.length : 0)
  }

  deletePanier(panier: any) {
    for (let i = 0; i < this.paniers.length; i++) {
      if (panier == this.paniers[i]) {
        this.paniers.splice(i, 1);
        localStorage.setItem('paniers', JSON.stringify(this.paniers));
        this.nombre--;
        this.change.emit(this.nombre);
      }
    }
  }

  public updateNbrePanier() {
    this.nombre = this.getProduitInPanier();
    this.change.emit(this.nombre);
  }

}
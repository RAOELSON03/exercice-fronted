import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriesComponent } from './categories/categories.component';
import { LoginComponent } from './login/login.component';
import { PaniersComponent } from './paniers/paniers.component';
import { DetailsProduitsComponent } from './produits/details-produits/details-produits.component';
import { UsersGuardService } from './services/users-guard.service';


const routes: Routes = [
  { path: '', component: CategoriesComponent, pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'panier', component: PaniersComponent, canActivate: [UsersGuardService] },
  { path: 'produit/:id', component: DetailsProduitsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

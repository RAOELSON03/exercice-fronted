import { Component, OnInit } from '@angular/core';
import { PanierService } from './services/panier.service';
import { AuthenticationService } from './services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  categories;
  title = 'exerciceFront';
  nbrePanier = 0;

  constructor(private panierService: PanierService,
    public authService: AuthenticationService,
    private router: Router) {

  }

  ngOnInit() {
    this.panierService.change.subscribe(data => {
      this.nbrePanier = data;
    });
    this.getProduitInPanier();
  }

  public getProduitInPanier() {
    this.nbrePanier = this.panierService.getProduitInPanier();
  }

  onLogin() {
    this.router.navigateByUrl('/login');
  }

  onLogout() {
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }

}

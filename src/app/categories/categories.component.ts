import { Component, OnInit } from "@angular/core";
import { CategorieService } from './../services/categories.service';

@Component({
  selector: "app-categories",
  templateUrl: './categories.component.html'
})

export class CategoriesComponent implements OnInit {
  categories: any;
  activeCategorie: any;
  
  constructor(private catService: CategorieService) {
  }

  ngOnInit(): void {
    this.getCategories();
  }

  public getCategories() {
    this.catService.getcategorieList().subscribe(data => {
      this.categories = data._embedded.categories;
    }, err => {
      console.log(err);
    })
  }

  getProduitByCat(c) {
    this.activeCategorie = c;
  }
}
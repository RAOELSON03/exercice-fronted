import { Component, OnInit } from '@angular/core';
import { PanierService } from './../services/panier.service';

@Component({
    selector: "app-paniers",
    templateUrl: './paniers.component.html'
})

export class PaniersComponent implements OnInit {
    paniers: any;
    constructor(private panierService: PanierService){

    }

    ngOnInit() {
        this.getProduits();
    }

    public getProduits() {
        this.paniers = this.panierService.getListPaniers();
    }

    public deletePanier(panier: any) {
        if(confirm('Are you sure you want to delete this product?')) {
            this.panierService.deletePanier(panier);
        }
    }
}